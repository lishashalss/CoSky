export interface ImportResponse {
  total: number;
  succeeded: number;
}
