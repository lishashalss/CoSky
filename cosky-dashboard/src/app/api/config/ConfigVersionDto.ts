export interface ConfigVersionDto {
  configId: string;
  version: number;
}
